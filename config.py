# Define the application directory
import os
import peewee

class Config(object):
    DEBUG = False
    TESTING = False
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    DATA_DIR = BASE_DIR + "/data/"
    DATABASE_CONNECT_OPTIONS = {}
    DATABASE_URI = os.path.join(DATA_DIR, "handin.db")
    CONSTANTS_URI = os.path.join(DATA_DIR, "constants.db")
    db        = peewee.SqliteDatabase(DATABASE_URI)
    constants = peewee.SqliteDatabase(CONSTANTS_URI)
    
    # Application threads. A common general assumption is
    # using 2 per available processor cores - to handle
    # incoming requests using one and performing background
    # operations using the other.
    THREADS_PER_PAGE = 4
    # Enable protection agains *Cross-site Request Forgery (CSRF)*
    CSRF_ENABLED     = True
    # Use a secure, unique and absolutely secret key for
    # signing the data.
    CSRF_SESSION_KEY = "secret"
    # Secret key for signing cookies
    SECRET_KEY = "secret"

class ProductionConfig(Config):
  pass

class DevelopmentConfig(Config):
    DEBUG = True

class TestingConfig(Config):
    TESTING = True

cfg = DevelopmentConfig()
