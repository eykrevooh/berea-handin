from flask import Flask, \
                  render_template, \
                  g
from peewee import *
from config import cfg

# Set up the app
app   = Flask(__name__)
app.config.from_object("config.DevelopmentConfig")
import admin

# Handle DB setup/teardown
@app.before_request
def before_request():
    g.db = cfg.db

@app.teardown_request
def teardown_request(exception):
  if (g.db is not None) and (not g.db.is_closed()):
    g.db.close()

# Move to error handling module.
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404
